Document ID: TII-16-1574

Document Title: SafeDrive: Online Driving Anomaly Detection from Large-Scale Vehicle Data

Keywords: Big Data, Driving Behavior, State Graph,Internet-of-Vehicles, OBD, Anomaly, Data Stream

Corresponding author: 
	full name: Tianyu Wo
	complete address: Haidian District, Xueyuan Road 37#, Beihang University, New Main Building, G514, Beijing, China
	email:woty@act.buaa.edu.cn
	telephone number: +86 138 1009 8886
	fax number: +86 10 8233 9274
Authors' names and IEEE and IES memberships
	Mingming Zhang 
	Chao Chen  Member 2010 No
	Tianyu Wo Member 2006 No
	Tao Xie Senior Member 2012 No
	Md Zakirul Alam Bhuiyan Member 2009 No
	Xuelian Lin Member 2006 No
IEEE Copyright is included, document name: CR_TII-16-1574
